/* Read data
% Below is the list of variables which get assigned by this load operation:
% 
% * D - number of documents - scalar 
% * N - total number of words - scalar
% * W - size of vocabulary (number of distinct words) - scalar
% * d - word to document mapping - 1 X N vector
% * w - word to vocabulary mapping - 1 X N vector
% * word - dictionary - W X 1 vector
*/
#include <fstream>
#include <iomanip>
#include <string>
#include <numeric>
#include <algorithm> 
#include <unistd.h>
#include <pthread.h>
#include <cmath>
#include <chrono>
#include <random>
#include <iostream>
#include <assert.h>
#include <vector>

using namespace std;

typedef struct my_param
{
	int currBatchDocStartId, currBatchDocEndId, procNo, iteration; 
}thread_params;

typedef struct my_param2
{
	int phiRowIdxStart, phiRowIdxEnd;
	double rho;
}thread_params_reduce;

double **N_theta, **N_phi, *N_Z, ***N_phi_hat, **N_Z_hat, **N_phi_normalized, **N_theta_normalized;
int *d, *w, *countVec, *doc_startIdx;
int K = 10; // Number of topics - input will override this
int M = 10; // Number of minibatches
double n_burn_ins = 1;
double alpha = 0.1;
double eta = 0.01;
double s_local = 1;
double tau_local = 10;
double kappa_local = 0.9;
double s_global = 1;
double tau_global = 0.5;
double kappa_global = 0.9;
double num_iter = 50;// Number of iterations - input will override this
int P = 12;
int C;
int D, W, N;

struct A {
    double num;
    int index;
};
struct Predicate {
    bool operator()(const A first, const A second) {
        return first.num > second.num;
    }
};

void freeAllVars()
{
	free(d);
	free(w);
	free(countVec);
	free(doc_startIdx);
	free(N_Z);
	for(unsigned i = 0; i < D; i++)
		free(N_theta[i]);
	free(N_theta);
	for(unsigned i = 0; i < W; i++)
		free(N_phi[i]);
	free(N_phi);
	for(unsigned i = 0; i < P; i++)
		free(N_Z_hat[i]);
	free(N_Z_hat);
	for(unsigned i = 0; i < P; i++)
	{	
		for(unsigned j =0; j < W; j ++)
			free(N_phi_hat[i][j]);
		free(N_phi_hat[i]);
	}
	free(N_phi_hat);
}

double getPerplexity()
{
	int curr_idx, doc_idx_minus_one;
	for (unsigned j = 0; j < K; ++ j) 
	{
		double sumCurrCol = 0;
		for (unsigned i = 0; i < W; ++ i)
		    sumCurrCol += N_phi[i][j];
		for (unsigned i = 0; i < W; ++ i)
		    N_phi_normalized[i][j] =  N_phi[i][j]/sumCurrCol;
	}
	for (unsigned i = 0; i < D; ++ i) 
	{
		double sumCurrRow = 0;
		for (unsigned j = 0; j < K; ++ j)
		    sumCurrRow += N_theta[i][j];
		for (unsigned j = 0; j < K; ++ j)
		    N_theta_normalized[i][j] =  N_theta[i][j]/sumCurrRow;
	}
	double logPerplexity = 0;
	for(unsigned doc_idx = 1; doc_idx <= D; doc_idx++)
	{
		int doc_start_idx = doc_startIdx[doc_idx-1];
		int doc_end_idx = -2, next_doc_idx = doc_idx;
		if (doc_start_idx == -1)
			continue;
		if (doc_idx + 1 > D)
		    doc_end_idx = N-1;
		else
		{
			while(doc_end_idx < 0)
			{
				doc_end_idx = doc_startIdx[next_doc_idx] - 1;
				next_doc_idx++;
			}
		}
		int C_j = 0;
		for(unsigned p = doc_start_idx; p <= doc_end_idx; p++)	
			C_j += countVec[p];
		for(int word_Idx = 0; word_Idx < doc_end_idx-doc_start_idx+1; word_Idx++)
		{
			curr_idx = word_Idx + doc_start_idx;
			double dotP = 0;
			for(unsigned currTop = 0; currTop < K; currTop++)
			{
				dotP += N_theta_normalized[doc_idx - 1][currTop] * N_phi_normalized[w[curr_idx]][currTop];
			}
			logPerplexity -= (countVec[curr_idx] * log(dotP));
		}	
	}
	double perplexity = exp(logPerplexity/C);
	cout << "Perplexity = " << perplexity << endl;
	return perplexity;
}

void *updateThetaPhiHatZHat(void *arg)
{
	double rho, oldValues_weight, gamma_i_j[K], gamma_i_j_sum;
	int curr_idx, doc_idx_minus_one;
	double etaMultW = eta*W, tempDoub;
	int p, q;
	int currBatchDocStartId = (*(thread_params*) arg).currBatchDocStartId;
	int currBatchDocEndId = (*(thread_params*) arg).currBatchDocEndId;
	int procNo = (*(thread_params*) arg).procNo;
	int iteration = (*(thread_params*) arg).iteration;
	for(int doc_idx = currBatchDocStartId; doc_idx <= currBatchDocEndId; doc_idx++)//currBatchDocEndId
	{
		int doc_start_idx = doc_startIdx[doc_idx-1];
		int doc_end_idx = -2, next_doc_idx = doc_idx;
		if (doc_start_idx == -1)
			continue;
		if (doc_idx + 1 > D)
		    doc_end_idx = N-1;
		else
		{
			while(doc_end_idx < 0)
			{
				doc_end_idx = doc_startIdx[next_doc_idx] - 1;
				next_doc_idx++;
			}
		}
		int C_j = 0;
		for(unsigned p = doc_start_idx; p <= doc_end_idx; p++)	
			C_j += countVec[p];
			
		double tmpVar = 0;
		for(unsigned i = 0; i < K; i++)
			tmpVar += N_theta[0][i];
		for(int burn_in_idx = 0; burn_in_idx < n_burn_ins; burn_in_idx++)
		{
			for(int word_Idx = 0; word_Idx < doc_end_idx - doc_start_idx + 1; word_Idx++)
			{
				curr_idx = word_Idx + doc_start_idx;
				rho = s_local/pow(tau_local +  word_Idx, kappa_local);
				oldValues_weight = pow(1 - rho, countVec[curr_idx]);
				gamma_i_j_sum = 0;
				
				doc_idx_minus_one = doc_idx-1;
				for (p=0; p < K; p++)
				{
					 gamma_i_j[p] = (N_phi[w[curr_idx]][p] + eta)*(N_theta[doc_idx_minus_one][p] + alpha)/(N_Z[p] + etaMultW);
					 gamma_i_j_sum += gamma_i_j[p];
			    }	  
			    tempDoub = C_j*(1 - oldValues_weight);          
				for (p=0; p < K; p++)
				{	
					N_theta[doc_idx_minus_one][p] = oldValues_weight*N_theta[doc_idx_minus_one][p] + (gamma_i_j[p]/gamma_i_j_sum)*tempDoub;
				} 
		    }
		}

		for(int word_Idx = 0; word_Idx < doc_end_idx-doc_start_idx+1; word_Idx++)
		{
			curr_idx = word_Idx + doc_start_idx;
			rho = s_local/pow(tau_local + word_Idx, kappa_local);
			oldValues_weight = pow(1 - rho,countVec[curr_idx]);
			gamma_i_j_sum = 0;					
			doc_idx_minus_one = doc_idx-1;
			for (p=0; p < K; p++)
			{
				 gamma_i_j[p] = (N_phi[w[curr_idx]][p] + eta)*(N_theta[doc_idx_minus_one][p] + alpha)/(N_Z[p] + etaMultW);
				 gamma_i_j_sum += gamma_i_j[p];
		   	}	                                     
		    tempDoub = C_j*(1 - oldValues_weight); 
	                
			for (p=0; p < K; p++)
			{	
				N_theta[doc_idx_minus_one][p] = oldValues_weight*N_theta[doc_idx_minus_one][p] + (gamma_i_j[p]/gamma_i_j_sum)*tempDoub;

				N_phi_hat[procNo][w[curr_idx]][p] += countVec[curr_idx]*(gamma_i_j[p]/gamma_i_j_sum);
				N_Z_hat[procNo][p] += countVec[curr_idx]*(gamma_i_j[p]/gamma_i_j_sum);
			}
		
		}
	}
}

void *reduce_N_Phi(void *arg)
{
	int phiRowIdxStart = (*(thread_params_reduce*) arg).phiRowIdxStart;
	int phiRowIdxend = (*(thread_params_reduce*) arg).phiRowIdxEnd;
	double rho = (*(thread_params_reduce*) arg).rho;
	double scaling = rho * M;
	for(unsigned p = phiRowIdxStart; p <= phiRowIdxend; p++)
	{
		for (unsigned q = 0; q < K; q++)
		{ 
			double N_phi_hat_p_q = 0;
			for(unsigned proc = 0; proc < P; proc++)
			{
				N_phi_hat_p_q += N_phi_hat[proc][p][q];
				N_phi_hat[proc][p][q] = 0;
			}
			N_phi[p][q] = (1 - rho)*N_phi[p][q] + scaling*N_phi_hat_p_q;
		}
	}
}

void generateOutputFiles()
{
	getPerplexity();
	ofstream topicsF;
	topicsF.open("topics.txt");
	for(unsigned j = 0; j < K; j++)
	{
		vector<A> currTopicWords;
		for(int i = 0; i < W; i++)
		{	
			A a = A();
			a.num = N_phi_normalized[i][j];
			a.index = i + 1;
			currTopicWords.push_back(a);
		}
		sort(currTopicWords.begin(), currTopicWords.end(), Predicate());
		for(int i = 0; i < 100; i++)
		{
			A a = currTopicWords[i];
			topicsF << a.index << ":" << setprecision(6) << fixed << a.num;
			if(i < 99)
				topicsF << ", ";
		}
		topicsF << endl;
	}
	topicsF.close();
	ofstream docTopic;
	docTopic.open("doctopic.txt");
	for(unsigned i = 0; i < D; i++)
	{
		for(unsigned j = 0; j < K; j++)
		{	
			docTopic << setprecision(6) << fixed << N_theta_normalized[i][j];
			if(j < K - 1)
				docTopic << ", ";
		}
		docTopic << endl;
	}
	docTopic.close();
}
int main(int argc, char *argv[]){
	
	if(argc != 4)
	{
		cout << "Please provide 3 input arguments in the form ./fastLDA docword.txt iterations NumOfTopics" << endl;
		return -1;
	}
	
	ifstream docword(argv[1]);
	num_iter = atoi(argv[2]);
	K = atoi(argv[3]);
	int a,b,c;
	docword >> D;
	docword >> W;
	docword >> N;

	cout << "P: " << P  << ", K: " << K << ", D: " << D <<", W: "<< W <<", N: "<< N << endl;
	cout << "Allocating memory to variables" << endl;
	N_theta = (double **)malloc(D * sizeof(double *));
    for (unsigned i=0; i<D; i++)
         N_theta[i] = (double *)malloc(K * sizeof(double));
		 
	N_theta_normalized = (double **)malloc(D * sizeof(double *));
    for (unsigned i=0; i<D; i++)
         N_theta_normalized[i] = (double *)malloc(K * sizeof(double));

	N_Z = (double *)malloc(K * sizeof(double));


	N_Z_hat = (double **)malloc(P * sizeof(double *));
	for(unsigned i = 0; i < P; i++)
		N_Z_hat[i] = (double *)malloc(K * sizeof(double));


	N_phi = (double **)malloc(W * sizeof(double *));
    for (unsigned i=0; i<W; i++)
         N_phi[i] = (double *)malloc(K * sizeof(double));
		 
	N_phi_normalized = (double **)malloc(W * sizeof(double *));
    for (unsigned i=0; i<W; i++)
         N_phi_normalized[i] = (double *)malloc(K * sizeof(double));

 	
	N_phi_hat = (double ***)malloc(P * sizeof(double**));
	assert(N_phi_hat != NULL);
    for (unsigned i=0; i < P; i++)
	{
         N_phi_hat[i] = (double **)malloc(W * sizeof(double *));
		 assert(N_phi_hat[i] != NULL);
		 for(unsigned j=0; j< W; j++)
		 {
			N_phi_hat[i][j] = (double *)malloc(K * sizeof(double));
			assert(N_phi_hat[i][j] != NULL);
		 }
	}
	
	cout << "Reading words from file" << endl;
	d = (int *)malloc(N * sizeof(int));
	w = (int *)malloc(N * sizeof(int));
	countVec = (int *)malloc(N * sizeof(int));
	
	int row = 0;
    while (docword >> a >> b >> c)
    {
		d[row] = a;
		w[row] = b-1;
		countVec[row] = c;
		row++;
    }
	
	cout << "d, w and countVec initialized" << endl;
 
	C = 0; // Number of words
	
	for(int i=0;i < N; i++)	
		C += countVec[i];	
	
	cout << "Total number of Words: " << C << endl;
	
	
	// Get doc start indices
	doc_startIdx = (int *)malloc(D * sizeof(int));
	for(int i=0; i< D; i++)
		doc_startIdx[i] = -1;

	doc_startIdx[0] = 0;
	for (int d_Idx = 0; d_Idx < N; d_Idx++){
	    if (d_Idx == N-1 || d[d_Idx] < d[d_Idx + 1]){
		if (d_Idx != N-1){
		    doc_startIdx[d[d_Idx+1]-1] = d_Idx + 1;
		}
	    }
	}
	
	cout << "Initializing N_theta, N_phi and N_Z" << endl;
	// Initialization
	// N_theta is the expected topic counts per document, D X K
	unsigned seed = chrono::system_clock::now().time_since_epoch().count();
  	default_random_engine generator (seed);
	uniform_real_distribution<double> distribution (0.0,1.0);
	
	for (int i = 0; i < D; i++)
		for(int j = 0; j < K; j++)
			N_theta[i][j] = distribution(generator);
	
	for (unsigned i = 0; i < D; ++ i) 
	{
		double sumCurrRow = 0;
		for (unsigned j = 0; j < K; ++ j)
		    sumCurrRow += N_theta[i][j];
		for (unsigned j = 0; j < K; ++ j)
		    N_theta[i][j] = (K*alpha + C/D)*N_theta[i][j]/sumCurrRow;
	}

	// N_Z is the expected topic counts overall, 1 X K
	for(int i = 0; i < K; i++)
	{
		double sumCurrCol=0;
		for (unsigned j = 0; j < D; ++ j)
			sumCurrCol += N_theta[j][i];
		N_Z[i] = sumCurrCol;
	}
	
	// N_phi is the expected topic counts per word, W X K
    for (int i=0; i<W; i++)
		for(int j=0; j<K; j++)
				N_phi[i][j] = distribution(generator);
	for (unsigned j = 0; j < K; ++ j) 
	{
		double sumCurrCol=0;
		for (unsigned i = 0; i < W; ++ i)
		{
			sumCurrCol += N_phi[i][j];
		}
		for (unsigned i = 0; i < W; ++ i)
		    N_phi[i][j] = (N_phi[i][j]/sumCurrCol)*N_Z[j];
	}
	cout << "N_phi initialized" << endl;

	for(unsigned proc = 0; proc < P; proc++)
	{
		for(int p=0; p<K; p++)
		{
			for(int q=0; q<W; q++)
			{
				N_phi_hat[proc][q][p]=0;
			}
			N_Z_hat[proc][p] = 0;
		}
	}
	cout << "N_phi_hat and N_Z_hat initialized" << endl;
	double rho, oldValues_weight, gamma_i_j_sum;
	int curr_idx, doc_idx_minus_one;
	double etaMultW = eta*W, tempDoub;
	int p, q;
	pthread_t thd[P];
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	cout << endl << "--------------------------------------------------" << endl << endl;
	
	for(int i = 0; i < num_iter; i++)
	{
	    
	    cout << "Running Iteration " << i+1 << endl;
	    for(int miniBatchIdx = 1; miniBatchIdx <= M; miniBatchIdx++)
		{
			int currBatch_doc_start_idx = floor(((miniBatchIdx - 1)*D)/M) + 1;
			int currBatch_doc_end_idx = floor((miniBatchIdx*D)/M);
			int numDocsCurrBatch = currBatch_doc_end_idx - currBatch_doc_start_idx + 1;
			int numDocsPerProc = numDocsCurrBatch/P;
			for(unsigned procNo = 0; procNo < P; procNo++)
			{	
				thread_params *curr_thread_params = (thread_params *)malloc(sizeof(thread_params));
				(*curr_thread_params).currBatchDocStartId = currBatch_doc_start_idx + procNo*numDocsPerProc;
				if(procNo == P - 1)
					(*curr_thread_params).currBatchDocEndId = currBatch_doc_end_idx;
				else
					(*curr_thread_params).currBatchDocEndId = currBatch_doc_start_idx + (procNo+1)*numDocsPerProc - 1;
				(*curr_thread_params).procNo = procNo;
				(*curr_thread_params).iteration = i;
				pthread_create(&thd[procNo], &attr, updateThetaPhiHatZHat, (void *)curr_thread_params);
			}
			for(unsigned procNo = 0; procNo < P; procNo++)
			{
				pthread_join(thd[procNo],NULL);
			}
			rho = s_global/pow(tau_global*M + i*M + miniBatchIdx, kappa_global);

			int noRowPerProc = W/P;
			for(unsigned procNo = 0; procNo < P; procNo++)
			{
				thread_params_reduce *curr_thread_reduce_params = (thread_params_reduce *)malloc(sizeof(thread_params_reduce));
				(*curr_thread_reduce_params).phiRowIdxStart = procNo*noRowPerProc;
				if(procNo == P - 1)
					(*curr_thread_reduce_params).phiRowIdxEnd = W - 1;
				else
					(*curr_thread_reduce_params).phiRowIdxEnd = (procNo + 1)*noRowPerProc - 1;
				(*curr_thread_reduce_params).rho = rho;
				pthread_create(&thd[procNo], &attr, reduce_N_Phi, (void *)curr_thread_reduce_params);
			}
			for(unsigned procNo = 0; procNo < P; procNo++)
			{
				pthread_join(thd[procNo],NULL);
			}
			
			for (q=0; q < K; q++)
			{
				double N_Z_hat_q = 0;
				for(unsigned proc = 0; proc < P; proc++)
				{
					N_Z_hat_q += N_Z_hat[proc][q];
					N_Z_hat[proc][q] = 0;
				}
				N_Z[q] = (1 - rho)*N_Z[q] + rho*M*N_Z_hat_q;
			}
		}
//		if(i % 5 == 0)
//			getPerplexity();
	}
	generateOutputFiles();
	freeAllVars();
	cout << "Processing Complete" << endl;
	return 0;
}

